import 'package:flutter/widgets.dart';

// ignore: must_be_immutable
class CustomImage extends StatelessWidget {
  CustomImage({Key key}) : super(key: key);

  double _screenWidth;

  @override
  Widget build(BuildContext context) {
    _screenWidth = MediaQuery.of(context).size.width;
    return Flexible(
      child: SizedBox.expand(
          child: Image.asset(
              "assets/images/caballos.jpg",
            fit: BoxFit.fitHeight,
          )
      ),
    );
  }
}
