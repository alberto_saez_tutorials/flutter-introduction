import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class LeftColumn extends StatelessWidget {
  LeftColumn({Key key}) : super(key: key);

  double _screenWidth;
  double _screenHeight;

  @override
  Widget build(BuildContext context) {
    _screenWidth = MediaQuery.of(context).size.width;
    _screenHeight = MediaQuery.of(context).size.height;
    return Container(
      width: _screenWidth * 0.4,
      height: _screenHeight,
      color: Colors.blue,
      child: Center(
        child: Container(
          height: 150,
          width: 150,
          child: Stack(
              children: [
            SizedBox(
              height: 100,
              width: 100,
              child: Container(color: Colors.amber),
            ),
            Positioned(
              top: 50,
              left: 50,
              child: SizedBox(
                height: 100,
                width: 100,
                child: Container(color: Colors.black),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
