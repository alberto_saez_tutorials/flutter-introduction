import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'counter_state.dart';

class TextNumber extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int counter = CounterState.of(context).counter;
    return Text(
      '$counter',
      style: Theme
          .of(context)
          .textTheme
          .headline4,
    );
  }
}
