import 'package:curso_flutter/inherited_widget/text_number.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ColumnNumber extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'You have pushed the button this many times:',
        ),
        TextNumber(),
      ],
    );
  }
}
