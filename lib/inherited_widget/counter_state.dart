import 'package:flutter/cupertino.dart';

// ignore: must_be_immutable
class CounterState extends InheritedWidget {

  int counter = 0;

  CounterState({@ required this.counter, Widget child}):
        super(child: child);

  static CounterState of(BuildContext context) =>
        context.dependOnInheritedWidgetOfExactType<CounterState>();

  @override
  bool updateShouldNotify(CounterState oldWidget) =>
      oldWidget.counter != this.counter;

}