import 'package:curso_flutter/hero/main_hero.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class HeroDetail extends StatelessWidget {
  Widget hero;
  HeroDetail({@required this.hero});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Hero Image"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(
              builder: (build) => MyHomePage(title: 'Flutter Demo Home Page',)
            ));
          },
        ),
      ),
      body: Container(
        child: hero
      ),
    );
  }
}
