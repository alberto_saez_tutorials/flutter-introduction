import 'package:curso_flutter/hero/hero_detail.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  final List<CustomTileData> listTilesMocked = [
    CustomTileData(
        iconPath: "assets/images/caballos.jpg",
        tag: "horse",
        title: "Foto de caballos"),
    CustomTileData(
        iconPath: "assets/images/comida.jpg",
        tag: "food",
        title: "Foto de comida")
  ];

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double _screenWidth;

  @override
  Widget build(BuildContext context) {
    _screenWidth = MediaQuery.of(context).size.width;

    Hero heroHorse =
        Hero(tag: "horse", child: Image.asset("assets/images/caballos.jpg"));
    Hero heroFood =
        Hero(tag: "food", child: Image.asset("assets/images/comida.jpg"));
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            ListTile(
              title: Text("Foto de comida"),
              leading: Container(
                width: _screenWidth * 0.2,
                child: heroFood,
              ),
              onTap: () {
                _goToHero(context, heroFood);
              },
            ),
            ListTile(
              title: Text("Foto de caballos"),
              leading: Container(width: _screenWidth * 0.2, child: heroHorse),
              onTap: () {
                _goToHero(context, heroHorse);
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _getRecyclerList() => ListView.builder(
        itemCount: widget.listTilesMocked.length,
        shrinkWrap: true,
        itemBuilder: (context, position) {
          var data = widget.listTilesMocked[position];
          var hero =  Hero(tag: data.tag, child: Image.asset(data.iconPath));
          return ListTile(
            title: Text(data.title),
            leading: Container(
              width: _screenWidth * 0.2,
              child: hero,
            ),
            onTap: () {
              _goToHero(context, hero);
            },
          );
        },
      );

  _goToHero(BuildContext context, Widget hero) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (build) => HeroDetail(
              hero: hero,
            )));
  }
}

class CustomTileData {
  String iconPath;
  String tag;
  String title;

  CustomTileData({this.iconPath = "", this.tag = "", this.title = ""});
}
